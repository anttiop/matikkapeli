import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  num1: number;
  num2: number;
  vastaus: number;

  constructor(public alertController: AlertController) {}

ngOnInit() {
  this.arvoLuvut();
}

laske() {
  if (this.num1 + this.num2 === this.vastaus) {
    this.viesti('Vastaus on oikein!');
  } else {
    this.viesti('Vastaus on väärin!');
  }
  this.arvoLuvut();

}

private async viesti(teksti: string) {
  const alert = await this.alertController.create({
    header: 'Matikkapeli',
    subHeader: '',
    message: teksti,
    buttons: ['OK']
  });

  await alert.present();   
}

private arvoLuvut() {
  this.num1 = Math.floor((Math.random() * 10) + 1);
  this.num2 = Math.floor((Math.random() * 10) + 1);
  this.vastaus = 0;
}

}
